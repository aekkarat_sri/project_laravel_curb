<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.master');
});
Route::get('/list', function () {
    return view('list');
});
Route::get('/add', function () {
    return view('add');
});

Route::resource('people', 'PeopleController');
// Route::view('/navbar','layouts.navbar');
// Route::get('/user', function () {
//     return view('user');
// });

// Route::view('/user', 'user');
// Route::get('users/{id}', function ($id) {
//     return view('users.user')->with('id',$id);
// });

// Route::get('users/{id}','UserController@index');

// Route::prefix('users')->group(function(){
//     Route::get('{id}','UserController@index');
//     Route::get('{id}/test', function() {
//         return "test";
//     })->where('id','[0-9]+');
// });