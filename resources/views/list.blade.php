@extends('layouts.master')
@section('css')
    @parent
    <style>
    </style>
@endsection
@section('title', 'list')
@section('content')
 @if (Session::has('message'))
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{ Session::get('message')}}
        </div>

 @endif
<table class="table">
    <thead class="thead-dark">
        <th>ID</th>
        <th>FName</th>
        <th>LName</th>
        <th>Age</th>
        <th>Time create</th>
        <th>Time update</th>
        <th>Action</th>
    </thead>
    <tbody>
    @foreach ($people as $p)
        <tr>
        <td>{{$p->id}}</td>
        <td>{{$p->fname}}</td>
        <td>{{$p->lname}}</td>
        <td>{{$p->age}}</td>
        {{-- <td>{{$p->created_at}}</td>
        <td>{{$p->updated_at}}</td> --}}
        <td>{{ date('d-m-Y  [H:i:s]', strtotime($p->created_at)) }}</td>
        <td>{{ date('d-m-Y  [H:i:s]', strtotime($p->updated_at)) }}</td>
        <td> 
            <div class="form-inline" >
                <a href="{{ url('people/' . $p->id . '/edit') }}">
                    <button type="button" class="btn btn-warning">edit</button>
                </a>
            
                <form action="{{ url('people/' . $p->id ) }}" method="post">
                    @csrf
                    @method('delete')
                        {{-- <a href="{{ url('people/'.$p->id.'/delete')}}"> --}}
                            <button type="submit" class="btn btn-danger">delete</button></a>
                </form>
            </div>
        </td>
        </tr>
    @endforeach

    </tbody>
</table>
<a href="{{ url('people/create') }}">
    <button type="button" class="btn btn-primary btn-lg btn-block">Create</button>
</a>

@endsection

@section('js')
    <script>
window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove(); 
    });
}, 6000);
    </script>
@endsection