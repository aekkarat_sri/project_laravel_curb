@extends('layouts.master')
@section('title', 'edit')
@section('content')
<form action="{{ url('people', [$people->id]) }}" method="post">
    @csrf
    @method('put')
    <h1>Edit ja</h1>
    <div class="form-group">
        <label>firstname</label>
        <input class="form-control" type="text" name="fname" value="{{$people->fname}}">
        <label>lastname</label>
    <input class="form-control" type="text" name="lname" value="{{$people->lname}}">
    </div>
    <div class="form-group">
            <label>age</label>
            <input class="form-control" type="text" name="age" value="{{$people->age}}">
    </div>
        <button type="submit" class="btn btn-success">save</button>
    <br><br>    
   
    @if($errors->any())
    <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>
                        {{ $error}}
                    </li>
                @endforeach
            </ul>
        @endif
    </div>
        
    </form>

@endsection