@extends('layouts.master')
@section('title', 'add')
@section('content')
<form action="{{ url('people') }}" method="post">
    @csrf
    <div class="form-group">
        <label>firstname</label>
        <input class="form-control" type="text" name="fname">
        <label>lastname</label>
        <input class="form-control" type="text" name="lname">
    </div>
    <div class="form-group">
            <label>age</label>
            <input class="form-control" type="text" name="age">
    </div>
        <button type="submit" class="btn btn-success">save</button>
    <br><br>    
   
    @if($errors->any())
    <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>
                        {{ $error}}
                    </li>
                @endforeach
            </ul>
        @endif
    </div>
        
    </form>

@endsection